const express = require('express')
const app = express()
const port = 3000
const router = require('./routes/route')
const path = require('path')
const session = require('express-session')
const flash = require('express-flash')
const passport = require('passport')
const LocalStrategy = require('passport-local').Strategy
const model = require('./models')
const bcrypt = require('bcrypt')



app.set('view engine','ejs')
app.set("views", path.join(__dirname, "views"))


app.use (session({
  secret : 'Rahasia',
  resave:true,
  saveUninitialized:true
}))
app.use(express.static(__dirname + '/public'));
//app.use(express.static(__dirname + '/public2'));
passport.use(  
  new LocalStrategy(
  {
    usernameField :"username",
    passwordField: "password" 

  }, 
  (username,password,done) => {
    console.log("Masuk ke server")
      model.User.findOne({
        where : {
          username:username,
        }
        
      }).then(User =>
        {
       
        if (User.dataValues ){
          //console.log("Masuk ke server")
          if(bcrypt.compareSync(password, User.dataValues.password)){
            return done (null, User)
          }else{
            return done (null, false , {
              message: 'Password Salah '
            })
          }
        }else{
          return done (null, false,{
            message: 'Username salah'
          })
        }
      })
  }
)
)

passport.serializeUser((user, done) => {
  done(null, user);
});
passport.deserializeUser((obj, done) => {
  done(null, obj);
});



app.use(passport.initialize())
app.use(passport.session())




app.use (flash())
app.use (express.json())
// app.use (express.urlencoded())
app.use(express.urlencoded({ extended: false }))
app.use(express.query())

app.use('/',router)


app.listen(port, () => {
  console.log(`Example app listening at http://localhost:${port}`)
})


module.exports = app 