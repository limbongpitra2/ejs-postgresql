const express = require('express')

const router = express.Router()

const app = require('../server')

const controller = require('../controllers/controller.js')

const Authenticated = (req,res,next)=>{
    if (req.isAuthenticated() ){
        next()
    }else{
        res.redirect('/login')
    }
};
//router.use ()

router.get('/users',controller.getUser)


router.get('/register',controller.getRegister)
router.post('/register',controller.register)

router.post('/login',controller.postLogin)
router.get('/login',controller.getLogin)



router.get('/dashboard',Authenticated,controller.getDashboard)


router.get('/logout',controller.getLogout)


router.get('/log',controller.getLog)
router.get('/reg',controller.getReg)

router.get ('/blog',Authenticated,controller.getBlog)
router.post ('/blog',controller.postBlog)


//FORM ACTION
router.get('/view/:id',controller.getView)
router.get('/update/:id',controller.getUpdate)
router.get('/delete/:id',controller.getDelete)


module.exports= router